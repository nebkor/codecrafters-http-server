use std::{
    env,
    fs::{File, OpenOptions},
    io::{Read, Write},
    path::{Path, PathBuf},
};

use anyhow::Result;
use itertools::Itertools;
use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::TcpListener,
};

const NOT_FOUND: &str = "HTTP/1.1 404 Not Found";
const OK: &str = "HTTP/1.1 200 OK";
const SEP: &str = "\r\n";
const DSEP: &str = "\r\n\r\n";

#[tokio::main]
async fn main() -> Result<()> {
    let listener = TcpListener::bind("127.0.0.1:4221").await?;
    let directory = get_directory();
    loop {
        let (mut stream, _) = listener.accept().await?;

        let dir = directory.clone();
        tokio::spawn(async move {
            let mut buf = [0; 2048];
            let _bites = stream.read(&mut buf).await.unwrap_or(0);
            let req = String::from_utf8_lossy(&buf);
            let lines = req.split(DSEP).collect_vec();
            let headers = lines[0].split(SEP).collect_vec();
            let body = lines[1];

            let (action, path) = parse_head(&headers);
            let resp = match path {
                "/" => slash(),
                p if p.starts_with("/echo/") => {
                    let echo = parse_echo(p).unwrap();
                    mk_ok_plain(&echo)
                }
                "/user-agent" => match parse_agent(&headers) {
                    Some(agent) => mk_ok_plain(&agent),
                    _ => mk_404(),
                },
                p if p.starts_with("/files/") => {
                    if let Some(dir) = dir {
                        let file = parse_files(p);
                        let mut path = dir.clone();
                        path.push(file);
                        let action = action.to_ascii_lowercase();
                        let action = action.as_str();
                        match action {
                            "get" => handle_get_file(&path),
                            "post" => handle_post_file(&path, &headers, body.as_bytes()),
                            _ => mk_404(),
                        }
                    } else {
                        mk_404()
                    }
                }
                _ => mk_404(),
            };
            stream.write_all(&resp).await.unwrap();
        });
    }
}

fn get_directory() -> Option<PathBuf> {
    let args = env::args().collect_vec();
    for (i, arg) in args.iter().enumerate() {
        if arg == "--directory" {
            let dndx = i + 1;
            return args.get(dndx).map(PathBuf::from);
        }
    }
    None
}

fn parse_head<'req>(req: &'req [&str]) -> (&'req str, &'req str) {
    req.first()
        .map(|line| {
            let mut line = line.split_ascii_whitespace().take(2);
            let action = line.next().unwrap();
            let path = line.next().unwrap();
            (action, path)
        })
        .unwrap()
}

fn handle_get_file(path: &Path) -> Vec<u8> {
    match File::open(path) {
        Ok(mut f) => {
            let mut buf = vec![];
            f.read_to_end(&mut buf).unwrap();
            mk_ok_octets(&buf)
        }
        _ => mk_404(),
    }
}

fn handle_post_file(path: &Path, headers: &[&str], body: &[u8]) -> Vec<u8> {
    let file = OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(path);

    let body = if let Some(len) = parse_content_length(headers) {
        &body[0..len]
    } else {
        body
    };

    match file {
        Ok(mut f) => {
            f.write_all(body).unwrap();
            f.flush().unwrap();
            f.sync_all().unwrap();
            format!("HTTP/1.1 201 Created{SEP}{SEP}").into_bytes()
        }
        _ => mk_404(),
    }
}

fn mk_ok_plain(body: &str) -> Vec<u8> {
    let len = body.len();
    format!("{OK}{SEP}Content-Type: text/plain{SEP}Content-Length: {len}{SEP}{SEP}{body}")
        .into_bytes()
}

fn mk_ok_octets(body: &[u8]) -> Vec<u8> {
    let len = body.len();
    let mut response = format!(
        "{OK}{SEP}Content-Type: application/octet-stream{SEP}Content-Length: {len}{SEP}{SEP}"
    )
    .into_bytes();
    response.extend_from_slice(body);
    response
}

fn slash() -> Vec<u8> {
    format!("{OK}{SEP}{SEP}").into_bytes()
}

fn mk_404() -> Vec<u8> {
    format!("{NOT_FOUND}{SEP}{SEP}").into_bytes()
}

fn parse_agent(headers: &[&str]) -> Option<String> {
    for &line in headers {
        let line = line.split(": ").collect_vec();
        if line.len() < 2 {
            continue;
        }
        let key = line[0];
        let val = line[1];
        if key.to_lowercase() == "user-agent" {
            return Some(val.to_string());
        }
    }
    None
}

fn parse_content_length(headers: &[&str]) -> Option<usize> {
    for &line in headers {
        let line = line.split(": ").collect_vec();
        if line.len() < 2 {
            continue;
        }
        let key = line[0];
        let val = line[1];
        if key.to_lowercase() == "content-length" {
            return val.parse().ok();
        }
    }
    None
}

fn parse_echo(path: &str) -> Option<String> {
    let req = path.trim_matches('/');
    let req = req.split('/').collect_vec();
    if let Some(&verb) = req.first() {
        if verb == "echo" {
            let rest = &req[1..];
            Some(rest.join("/"))
        } else {
            None
        }
    } else {
        None
    }
}

fn parse_files(path: &str) -> String {
    let req = path.trim_matches('/');
    let req = req.split('/').collect_vec();
    req[1..].join("/")
}
